import React from 'react';
import { StyleSheet, Text, View, Image, Divider } from 'react-native';

const steps = [
        {
            image: require('../images/step_1_photo.png'),
            arrow:require("../images/step_right_arrow.png"),
            description: "VIA THE MAIN ENTRANCE, TAKE A RIGHT AT RECEPTION.",
            number: "1"
        },
        {
            image: require("../images/step_2_photo.png"),
            arrow:require("../images/step_forward_arrow.png"),
            description: "GO STRAIGHT, PASS THE QUEEN ELIZABETH STATUE UNTIL YOU ARRIVE AT LIFT F.",
            number: "2"
        },
        {
            image: require("../images/step_3_photo.png"),
            arrow:require("../images/step_lift.png"),
            description: "USING LIFT F, GO UP To FLOOR 3.",
            number: "3"
        },
        {
            image: require("../images/step_1_photo.png"),
            arrow:require("../images/step_right_arrow.png"),
            description: "VIA THE MAIN ENTRANCE, TAKE A RIGHT AT RECEPTION.",
            number: "4"
        },
        {
            image: require("../images/step_2_photo.png"),
            arrow:require("../images/step_forward_arrow.png"),
            description: "GO STRAIGHT, PASS THE QUEEN ELIZABETH STATUE UNTIL YOU ARRIVE AT LIFT F.",
            number: "5"
        },
        {
            image: require("../images/step_3_photo.png"),
            arrow:require("../images/step_lift.png"),
            description: "USING LIFT F, GO UP To FLOOR 3.",
            number: "6"
        }
    ]

export default class Step extends React.Component {
    render() {
        let curStep = steps.find((e) => e.number === this.props.step)
        return (
            <View>
                <Image
                    source={curStep.image}
                />
                <Image
                    source={curStep.arrow}
                />
                <Text>
                    {curStep.description}
                </Text>

                
            </View>
        )
    }
}