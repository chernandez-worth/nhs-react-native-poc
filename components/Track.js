import React from 'react';
import {Text, View, FlatList, Image} from 'react-native';
import {NativeRouter, Route, Link} from 'react-router-native';

const users = [
        {
            name:"Test",
            step: '/',
            image: require("../images/track_right_arrow.png")
        },
        {
            name:"Test",
            step: '/stepTwo',
            image: require("../images/track_forward_arrow.png")
        },
        {
            name:"Test",
            step: '/stepThree',
            image: require("../images/track_lift.png")
        },
        {
            name:"Test",
            step: '/stepFour',
            image: require("../images/track_right_arrow.png")
        },
        {
            name:"Test",
            step: '/stepFive',
            image: require("../images/track_forward_arrow.png")
        },
        {
            name:"Test",
            step: '/stepSix',
            image: require("../images/track_lift.png")
        }
    ]
export default class Track extends React.Component {
    render(){
        return(
                <View>
                    <FlatList
                        data={users}
                        horizontal={true}
                        renderItem={({item, index}) =>
                        <Link
                            to={item.step}
                        >
                            <View>
                                <Image
                                    source={item.image}
                                />
                            </View>
                        </Link>
                        }
                        keyExtractor={item => item.step}
                    />
                    
                </View>
        )
    }
}