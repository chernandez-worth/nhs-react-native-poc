import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {Link} from 'react-router-native';
const styles= StyleSheet.create({
    footer:{
        width:'100%',
        height:39.3,
        backgroundColor:'#0272BC'
    },
    nextContainer:{
      width:"90%"
    },
    next:{
      fontSize: 17,
      fontWeight: "bold",
      fontStyle: "normal",
      letterSpacing: 0.75,
      textAlign: "right",
      color: "#ffffff"
    },
    back:{
      fontSize: 17,
      fontWeight: "bold",
      fontStyle: "normal",
      letterSpacing: 0.75,
      textAlign: "left",
      color: "#ffffff"
    }
})


export default class Footer extends React.Component {
    render() {
      return (
        <View style={styles.footer}>
          <Link
              to="/stepTwo"
          >
            <View style={styles.nextContainer}>
              <Text style={styles.next}>NEXT</Text>
            </View>
          </Link>
          <Link
              to="/"
          >
            <View style={styles.nextContainer}>
              <Text style={styles.back}>BACK</Text>
            </View>
          </Link> 
        </View>
      );
    }
  }