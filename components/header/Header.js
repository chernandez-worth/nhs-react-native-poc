import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

const styles= StyleSheet.create({
    header:{
        width:'100%',
        height:37.3,
        backgroundColor:'#0272BC'
    }
})

const nhs_logo = {
  source: require('../../images/nhs_logo.png'),
}
export default class Header extends React.Component {
    render() {
      return (
        <View style={styles.header}>
          <View> 
            <Image source={nhs_logo.source}/>
          </View>
          
        </View>
      );
    }
  }