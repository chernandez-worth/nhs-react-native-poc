import React from 'react';
import { ScrollView, StyleSheet, Text, View, Image } from 'react-native';
import {NativeRouter, Route, Link} from 'react-router-native';
import Header from './components/header/Header.js';
import Footer from './components/Footer.js';
import Track from './components/Track.js';
import Step from './components/Step.js';

const StepOne = () => (
  <View>
    <Step step="1" />
    <View style={styles.divider}>
      <View style={{backgroundColor: '#CED0CE', height: 1, flex: 1, alignSelf: 'center'}} />
      
    </View>
    <Text> Next: </Text>
    <Step step="2" />
  </View>
)
const StepTwo = () => (
  <View>
    <Step step="2"/>
    <View style={styles.divider}>
      <View style={{backgroundColor: 'black', height: 1, flex: 1, alignSelf: 'center', width:'86%'}} />
      
    </View>
    <Text> Next: </Text>
    <Step step="3" />
  </View>
)

const StepThree = () => (
  <View>
    <Step step="3"/>
    <View style={styles.divider}>
      <View style={{backgroundColor: 'black', height: 2, flex: 1, alignSelf: 'center'}} />
    </View>
    <Text> Next: </Text>
    <Step step="4" />
  </View>
)
const StepFour = () => (
  <View>
    <Step step="4"/>
    <View style={styles.divider}>
      <View style={{backgroundColor: 'black', height: 2, flex: 1, alignSelf: 'center'}} />
    </View>
    <Text> Next: </Text>
    <Step step="5" />
  </View>
)
const StepFive = () => (
  <View>
    <Step step="5"/>
    <View style={styles.divider}>
      <View style={{backgroundColor: 'black', height: 2, flex: 1, alignSelf: 'center'}} />
    </View>
    <Text> Next: </Text>
    <Step step="6" />
  </View>
)

const StepSix = () => (
  <View>
    <Step step="6"/>
    <View style={styles.divider}>
      <View style={{backgroundColor: 'black', height: 2, flex: 1, alignSelf: 'center'}} />
    </View>
  </View>
)

export default class App extends React.Component {
render() {
    return (
      
        <View style={styles.container}>
          <Header/>
          <NativeRouter>
              <ScrollView>
                <Track/>
                <Route exact path="/" component={StepOne}/>
                <Route path="/stepTwo" component={StepTwo}/>
                <Route path="/stepThree" component={StepThree}/>
                <Route path="/stepFour" component={StepFour}/>
                <Route path="/stepFive" component={StepFive}/>
                <Route path="/stepSix" component={StepSix}/>
              </ScrollView>
            </NativeRouter>
            
              <Footer/>
        </View>
        
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width:'100%',
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center', 
    justifyContent: 'center',
  },
  divider: {
    flexDirection: 'row',
    height: StyleSheet.hairlineWidth,
  },
});
